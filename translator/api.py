# -*- coding: utf-8 -*-
TOKEN_ENDPOINT = 'https://api.cognitive.microsoft.com/sts/v1.0/issueToken'
TRANSLATE_BASE = 'https://api.microsofttranslator.com/v2/http.svc'
TRANSLATE_ENDPOINT = TRANSLATE_BASE + '/Translate'
TRANSLATE_ARRAY_ENDPOINT = TRANSLATE_BASE + '/TranslateArray'
HTTP_TIMEOUT = 10  # seconds
TRANSLATE_ARRAY_REQUEST = '''
    <TranslateArrayRequest>
      <AppId />
      <Texts>
        %(texts)s
      </Texts>
      <To>%(to)s</To>
    </TranslateArrayRequest>
'''
TRANSLATE_ARRAY_REQUEST_VALUE = '<string xmlns="http://schemas.microsoft.com/2003/10/Serialization/Arrays">%s</string>'
RESPONSE_NAMESPACES = {'ns1': 'http://schemas.datacontract.org/2004/07/Microsoft.MT.Web.Service.V2',
                       'ns2': 'http://schemas.microsoft.com/2003/10/Serialization/Arrays'}
